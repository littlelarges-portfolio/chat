import 'package:chat/application/user/actor/user_actor_bloc.dart';
import 'package:chat/application/user/user_bloc.dart';
import 'package:chat/domain/auth/value_objects.dart';
import 'package:chat/domain/user/user.dart';
import 'package:chat/injection.dart';
import 'package:chat/presentation/auth/core/flushbar_messenger.dart';
import 'package:chat/presentation/core/colours.dart';
import 'package:chat/presentation/core/common_widgets/custom_button_widget.dart';
import 'package:chat/presentation/core/common_widgets/custom_loading_overlay_widget.dart';
import 'package:chat/presentation/core/common_widgets/custom_scaffold_drawer_widget.dart';
import 'package:chat/presentation/core/common_widgets/custom_text_form_field_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';

class ProfilePage extends HookWidget {
  const ProfilePage({super.key});

  static const route = '/profile';

  @override
  Widget build(BuildContext context) {
    final changed = useState(false);
    final nameController = useTextEditingController();
    final emailController = useTextEditingController();
    final name = useState('');
    final user = useState(User.empty());

    void updateProfile() {
      context.read<UserBloc>().state.maybeMap(
            updatedSuccess: (updatedSuccessState) {
              user.value = updatedSuccessState.user;

              changed.value = false;
              name.value = user.value.name.getOrCrash();
              nameController.text = name.value;
              emailController.text = user.value.email.getOrCrash();
            },
            orElse: () {},
          );
    }

    useEffect(
      () {
        updateProfile();

        return () {};
      },
      [nameController, emailController],
    );

    return BlocProvider(
      create: (context) => getIt<UserActorBloc>(),
      child: BlocConsumer<UserActorBloc, UserActorState>(
        listener: (context, userActorState) {
          userActorState.map(
            initial: (_) {},
            actionInProgress: (_) {},
            updateSuccess: (_) {
              context.read<UserBloc>().add(
                    UserEvent.updated(
                      user:
                          user.value.copyWith(name: Name(nameController.text)),
                    ),
                  );
              FlushbarMessenger.showSuccessMessage(
                context: context,
                message: 'Profile updated successfully',
              );
            },
            updateFailure: (_) {
              FlushbarMessenger.showFailureMessage(
                context: context,
                message: 'An error occurred while updating a profile',
              );
            },
          );
        },
        builder: (context, userActorState) {
          return BlocConsumer<UserBloc, UserState>(
            listener: (context, state) => updateProfile(),
            builder: (context, userState) {
              return Stack(
                children: [
                  CustomScaffoldDrawer(
                    padding: EdgeInsets.all(20.r),
                    appBarTitle: 'Profile',
                    body: Column(
                      children: [
                        CustomTextFormField(
                          controller: nameController,
                          hintText: 'Имя',
                          onChanged: (value) {
                            if (name.value != nameController.text) {
                              changed.value = true;
                            } else {
                              changed.value = false;
                            }
                          },
                        ),
                        Gap(20.r),
                        CustomTextFormField(
                          enabled: false,
                          hintText: emailController.text,
                        ),
                        Gap(20.r),
                        CustomButton(
                          text: 'Сохранить',
                          color: Colours.green,
                          textColor: Colours.white,
                          onTap: changed.value
                              ? () {
                                  context.read<UserActorBloc>().add(
                                        UserActorEvent.updated(
                                          user: user.value.copyWith(
                                            name: Name(nameController.text),
                                          ),
                                        ),
                                      );
                                }
                              : null,
                        ),
                      ],
                    ),
                  ),
                  userActorState.maybeMap(
                    actionInProgress: (_) => const CustomLoadingOverlayWidget(),
                    orElse: Container.new,
                  ),
                ],
              );
            },
          );
        },
      ),
    );
  }
}
