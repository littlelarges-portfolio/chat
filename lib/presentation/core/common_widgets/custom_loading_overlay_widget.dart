import 'package:chat/presentation/core/colours.dart';
import 'package:flutter/material.dart';

class CustomLoadingOverlayWidget extends StatelessWidget {
  const CustomLoadingOverlayWidget({
    this.backgroundColor,
    this.indicatorColor,
    super.key,
  });

  factory CustomLoadingOverlayWidget.black() => CustomLoadingOverlayWidget(
        indicatorColor: Colours.black,
        backgroundColor: Colours.transparent,
      );

  final Color? backgroundColor;
  final Color? indicatorColor;

  @override
  Widget build(BuildContext context) {
    return ColoredBox(
      color: backgroundColor ?? Colors.black.withOpacity(.4),
      child: Center(
        child: CircularProgressIndicator(
          color: indicatorColor ?? Colors.white,
        ),
      ),
    );
  }
}
