import 'package:chat/presentation/core/common_widgets/custom_button_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';

class CustomErrorWidget extends StatelessWidget {
  const CustomErrorWidget({
    required this.errorContent,
    required this.onRetry,
    super.key,
  });

  final String errorContent;
  final void Function() onRetry;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(errorContent),
          Gap(10.r),
          CustomButton(
            text: 'Retry',
            onTap: onRetry,
          ),
        ],
      ),
    );
  }
}
