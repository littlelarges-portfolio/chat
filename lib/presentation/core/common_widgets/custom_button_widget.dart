import 'package:chat/presentation/core/helpers.dart';
import 'package:chat/presentation/core/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({
    required this.text,
    required this.onTap,
    this.color,
    this.textColor,
    super.key,
  });

  final String text;
  final void Function()? onTap;
  final Color? color;
  final Color? textColor;

  double get _borderRadius => 12.r;
  bool get _disabled => onTap == null;

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        overlayColor: WidgetStatePropertyAll(color?.makeDarker(.3)),
        borderRadius: BorderRadius.circular(_borderRadius),
        onTap: onTap,
        child: Ink(
          padding: EdgeInsets.symmetric(vertical: 10.r, horizontal: 20.r),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(_borderRadius),
            color: _disabled ? Colors.grey : color,
          ),
          child: Text(
            text,
            style: TextStyles.gilroy14Medium(color: textColor),
          ),
        ),
      ),
    );
  }
}
