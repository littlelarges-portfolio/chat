import 'package:chat/application/auth/auth_bloc.dart';
import 'package:chat/presentation/core/colours.dart';
import 'package:chat/presentation/core/common_widgets/custom_drawer_item_widget.dart';
import 'package:chat/presentation/core/gen/assets.gen.dart';
import 'package:chat/presentation/core/routes/router.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';

class CustomDrawer extends StatelessWidget {
  const CustomDrawer({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        physics: const NeverScrollableScrollPhysics(),
        children: [
          const DrawerHeader(
            child: Text('Chat'),
          ),
          Gap(20.r),
          CustomDrawerItem(
            text: 'Chats',
            icon: Assets.icons.chat.svg(
              colorFilter: ColorFilter.mode(Colours.black, BlendMode.srcIn),
            ),
            onTap: () {
              Navigator.of(context).pop();

              ChatsOverviewRoute().go(context);
            },
          ),
          CustomDrawerItem(
            text: 'Users',
            icon: Assets.icons.users.svg(
              colorFilter: ColorFilter.mode(Colours.black, BlendMode.srcIn),
            ),
            onTap: () {
              Navigator.of(context).pop();

              UsersRoute().go(context);
            },
          ),
          CustomDrawerItem(
            text: 'Profile',
            icon: Assets.icons.profile.svg(
              colorFilter: ColorFilter.mode(Colours.black, BlendMode.srcIn),
            ),
            onTap: () {
              Navigator.of(context).pop();

              context.read<AuthBloc>().state.maybeMap(
                    authenticated: (authenticatedState) {
                      ProfileRoute().go(context);
                    },
                    orElse: () {},
                  );
            },
          ),
          Gap(20.r),
          CustomDrawerItem(
            text: 'Log Out',
            icon: Assets.icons.logout.svg(
              colorFilter: ColorFilter.mode(Colours.black, BlendMode.srcIn),
            ),
            onTap: () {
              context.read<AuthBloc>().add(const AuthEvent.signedOut());
            },
          ),
        ],
      ),
    );
  }
}
