import 'package:chat/presentation/core/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';

class CustomDrawerItem extends StatelessWidget {
  const CustomDrawerItem({
    required this.text,
    required this.icon,
    this.onTap,
    super.key,
  });

  final String text;
  final Widget icon;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        onTap: onTap,
        child: Ink(
          padding: EdgeInsets.all(
            20.r,
          ),
          child: Row(
            children: [
              icon,
              Gap(20.r),
              Text(
                text,
                style: TextStyles.gilroy14Medium(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
