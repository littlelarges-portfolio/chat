import 'package:chat/presentation/core/colours.dart';
import 'package:chat/presentation/core/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomTextFormField extends StatelessWidget {
  const CustomTextFormField({
    this.hintText,
    this.controller,
    this.onChanged,
    this.validator,
    this.textInputAction,
    this.onEditingComplete,
    this.enabled,
    super.key,
  });

  final TextEditingController? controller;
  final String? hintText;
  final ValueChanged<String>? onChanged;
  final String? Function(String?)? validator;
  final TextInputAction? textInputAction;
  final void Function()? onEditingComplete;
  final bool? enabled;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      enabled: enabled,
      controller: controller,
      style: TextStyles.gilroy16Medium(color: Colours.black),
      decoration: InputDecoration(
        filled: true,
        fillColor: Colours.stroke,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12.r),
          borderSide: BorderSide.none,
        ),
        hintText: hintText,
        hintStyle: TextStyles.gilroy16Medium(color: Colours.gray),
      ),
      onChanged: onChanged,
      validator: validator,
      textInputAction: textInputAction,
      onEditingComplete: onEditingComplete,
    );
  }
}
