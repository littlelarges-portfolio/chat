import 'package:chat/application/auth/auth_bloc.dart';
import 'package:chat/presentation/auth/core/auth_listener.dart';
import 'package:chat/presentation/core/common_widgets/custom_appbar_widget.dart';
import 'package:chat/presentation/core/common_widgets/custom_drawer_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CustomScaffoldDrawer extends StatelessWidget {
  const CustomScaffoldDrawer({
    this.body,
    this.appBarTitle,
    this.padding,
    super.key,
  });

  final Widget? body;
  final String? appBarTitle;
  final EdgeInsetsGeometry? padding;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(title: appBarTitle),
      drawer: const CustomDrawer(),
      body: BlocListener<AuthBloc, AuthState>(
        listener: authListener,
        child: Padding(
          padding: padding ?? EdgeInsets.zero,
          child: body,
        ),
      ),
    );
  }
}
