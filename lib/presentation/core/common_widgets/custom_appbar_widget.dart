import 'package:chat/presentation/core/colours.dart';
import 'package:chat/presentation/core/gen/assets.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class CustomAppBar extends StatelessWidget implements PreferredSizeWidget {
  const CustomAppBar({
    this.title,
    super.key,
  });

  final String? title;

  Widget? get _title => title != null ? Text(title!) : null;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: _title,
      leading: Builder(
        builder: (context) {
          return IconButton(
            icon: Assets.icons.menu.svg(
              colorFilter: ColorFilter.mode(Colours.black, BlendMode.srcIn),
            ),
            onPressed: () {
              Scaffold.of(context).openDrawer();
            },
          );
        },
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(56.r);
}
