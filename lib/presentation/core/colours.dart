import 'dart:ui';

abstract class Colours {
  static Color get transparent => const Color(0x00FFFDFC);
  static Color get background => const Color(0xFFFFFDFC);
  static Color get white => const Color(0xFFFFFFFF);
  static Color get black => const Color(0xFF000000);
  static Color get errorColor => const Color(0xFFFF3333);
  static Color get stroke => const Color(0xFFEDF2F6);
  static Color get gray => const Color(0xFF9DB7CB);
  static Color get green => const Color(0xFF3CED78);
  static Color get blue => const Color(0xFF4E7AE9);
}
