import 'package:chat/presentation/auth/sign_in/sign_in_page.dart';
import 'package:chat/presentation/auth/sign_up/sign_up_page.dart';
import 'package:chat/presentation/chat/chats_overview_page.dart';
import 'package:chat/presentation/profile/profile_page.dart';
import 'package:chat/presentation/splash/splash_page.dart';
import 'package:chat/presentation/users/users_page.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

part 'router.g.dart';

final router = GoRouter(routes: $appRoutes, initialLocation: SplashPage.route);

@TypedGoRoute<SplashRoute>(
  path: SplashPage.route,
)
class SplashRoute extends GoRouteData {
  SplashRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) => const SplashPage();
}

@TypedGoRoute<ChatsOverviewRoute>(
  path: ChatsOverviewPage.route,
)
class ChatsOverviewRoute extends GoRouteData {
  ChatsOverviewRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const ChatsOverviewPage();
}

@TypedGoRoute<UsersRoute>(
  path: UsersPage.route,
)
class UsersRoute extends GoRouteData {
  UsersRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) => const UsersPage();
}

@TypedGoRoute<ProfileRoute>(
  path: ProfilePage.route,
)
class ProfileRoute extends GoRouteData {
  ProfileRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      const ProfilePage();
}

@TypedGoRoute<SignInRoute>(
  path: SignInPage.route,
)
class SignInRoute extends GoRouteData {
  SignInRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) => const SignInPage();
}

@TypedGoRoute<SignUpRoute>(
  path: SignUpPage.route,
)
class SignUpRoute extends GoRouteData {
  SignUpRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) => const SignUpPage();
}
