import 'dart:ui';

extension ColorX on Color {
  Color makeDarker(double amount) {
    assert(
      amount >= 0 && amount <= 1,
      "Can't be greater than 1 and less then 0",
    );

    final red = (this.red * (1 - amount)).round();
    final green = (this.green * (1 - amount)).round();
    final blue = (this.blue * (1 - amount)).round();

    return Color.fromARGB(alpha, red, green, blue);
  }
}
