import 'package:chat/presentation/core/gen/fonts.gen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

abstract class TextStyles {
  static TextStyle gilroy14Medium({Color? color}) => TextStyle(
        fontFamily: FontFamily.gilroy,
        fontSize: 14.r,
        fontWeight: FontWeight.w500,
        color: color,
      );
      
  static TextStyle gilroy16Medium({Color? color}) => TextStyle(
        fontFamily: FontFamily.gilroy,
        fontSize: 16.r,
        fontWeight: FontWeight.w500,
        color: color,
      );

  static TextStyle gilroy20Medium({Color? color}) => TextStyle(
        fontFamily: FontFamily.gilroy,
        fontSize: 20.r,
        fontWeight: FontWeight.w500,
        color: color,
      );

  static TextStyle gilroy24Medium({Color? color}) => TextStyle(
        fontFamily: FontFamily.gilroy,
        fontSize: 24.r,
        fontWeight: FontWeight.w500,
        color: color,
      );
}
