import 'package:chat/application/user/user_bloc.dart';
import 'package:chat/presentation/core/common_widgets/custom_loading_overlay_widget.dart';
import 'package:chat/presentation/core/common_widgets/custom_scaffold_drawer_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';

class ChatsOverviewPage extends HookWidget {
  const ChatsOverviewPage({super.key});

  static const route = '/chats';

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldDrawer(
      appBarTitle: 'Chats',
      body: BlocBuilder<UserBloc, UserState>(
        builder: (context, userState) {
          return userState.map(
            updatedSuccess: (updatedSuccessState) => Center(
              child: Text(
                updatedSuccessState.user.name.getOrCrash(),
              ),
            ),
            initial: (_) => CustomLoadingOverlayWidget.black(),
          );
        },
      ),
    );
  }
}
