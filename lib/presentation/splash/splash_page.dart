// ignore_for_file: avoid_print

import 'package:chat/application/auth/auth_bloc.dart';
import 'package:chat/presentation/auth/core/auth_listener.dart';
import 'package:chat/presentation/core/common_widgets/custom_loading_overlay_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({super.key});

  static const route = '/splash';

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
      listener: authListener,
      child: Scaffold(
        body: CustomLoadingOverlayWidget.black(),
      ),
    );
  }
}
