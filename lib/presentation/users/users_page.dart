import 'package:chat/application/users/watcher/users_watcher_bloc.dart';
import 'package:chat/injection.dart';
import 'package:chat/presentation/core/colours.dart';
import 'package:chat/presentation/core/common_widgets/custom_error_widget.dart';
import 'package:chat/presentation/core/common_widgets/custom_loading_overlay_widget.dart';
import 'package:chat/presentation/core/common_widgets/custom_scaffold_drawer_widget.dart';
import 'package:chat/presentation/core/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class UsersPage extends StatelessWidget {
  const UsersPage({super.key});

  static const route = '/users';

  @override
  Widget build(BuildContext context) {
    return CustomScaffoldDrawer(
      appBarTitle: 'Users',
      body: BlocProvider(
        create: (context) => getIt<UsersWatcherBloc>()
          ..add(const UsersWatcherEvent.watchStarted()),
        child: BlocBuilder<UsersWatcherBloc, UsersWatcherState>(
          builder: (context, usersWatcherState) {
            return usersWatcherState.map(
              initial: (_) => Container(),
              loadInProgress: (_) => CustomLoadingOverlayWidget.black(),
              loadSuccess: (loadSuccessState) {
                final users = loadSuccessState.users;

                return ListView.builder(
                  itemBuilder: (context, index) {
                    final user = users.get(index);
                    return Material(
                      type: MaterialType.transparency,
                      child: InkWell(
                        onTap: () {},
                        child: Ink(
                          padding: EdgeInsets.symmetric(vertical: 10.r),
                          child: ListTile(
                            title: Text(
                              user.name.getOrCrash(),
                              style: TextStyles.gilroy16Medium(),
                            ),
                            leading: CircleAvatar(
                              radius: 30.r,
                              backgroundColor: Colours.blue,
                              child: Text(
                                user.name.getOrCrash().substring(0, 1),
                                style: TextStyles.gilroy24Medium(
                                  color: Colours.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    );
                  },
                  itemCount: users.size,
                );
              },
              loadFailure: (loadFailureState) => CustomErrorWidget(
                errorContent: loadFailureState.failure.map(
                  serverError: (_) => 'Something went wrong',
                  unexpected: (_) => 'Unexpected error occurred',
                ),
                onRetry: () {},
              ),
            );
          },
        ),
      ),
    );
  }
}
