import 'package:chat/application/auth/sign_in_form/sign_in_form_bloc.dart';
import 'package:chat/injection.dart';
import 'package:chat/presentation/auth/sign_in/widgets/sign_in_form.dart';
import 'package:chat/presentation/core/common_widgets/custom_loading_overlay_widget.dart';
import 'package:chat/presentation/core/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SignInPage extends StatelessWidget {
  const SignInPage({super.key});

  static const route = '/sign_in';

  double get _padding => 10.r;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<SignInFormBloc>(),
      child: BlocBuilder<SignInFormBloc, SignInFormState>(
        buildWhen: (previous, current) =>
            previous.isSubmiting != current.isSubmiting,
        builder: (context, signInFormState) {
          return Stack(
            children: [
              Scaffold(
                appBar: AppBar(
                  title: Text(
                    'Chat',
                    style: TextStyles.gilroy20Medium(),
                  ),
                  centerTitle: true,
                ),
                body: Padding(
                  padding: EdgeInsets.symmetric(horizontal: _padding),
                  child: SingleChildScrollView(
                    child: SizedBox(
                      height: 1.sh -
                          kToolbarHeight -
                          kTextTabBarHeight -
                          kBottomNavigationBarHeight -
                          _padding,
                      width: 1.sw,
                      child: const Column(
                        children: [
                          SignInForm(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              if (signInFormState.isSubmiting)
                const CustomLoadingOverlayWidget(),
            ],
          );
        },
      ),
    );
  }
}
