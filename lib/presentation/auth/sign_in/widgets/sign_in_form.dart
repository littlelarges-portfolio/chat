// ignore_for_file: inference_failure_on_instance_creation

import 'package:chat/application/auth/auth_bloc.dart';
import 'package:chat/application/auth/sign_in_form/sign_in_form_bloc.dart';
import 'package:chat/presentation/auth/core/flushbar_messenger.dart';
import 'package:chat/presentation/auth/core/helpers.dart';
import 'package:chat/presentation/auth/sign_up/sign_up_page.dart';
import 'package:chat/presentation/core/colours.dart';
import 'package:chat/presentation/core/common_widgets/custom_button_widget.dart';
import 'package:chat/presentation/core/common_widgets/custom_text_form_field_widget.dart';
import 'package:chat/presentation/core/routes/router.dart';
import 'package:chat/presentation/core/text_styles.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:go_router/go_router.dart';

class SignInForm extends HookWidget {
  const SignInForm({super.key});

  @override
  Widget build(BuildContext context) {
    final emailController = useTextEditingController();
    final passwordController = useTextEditingController();

    return BlocConsumer<SignInFormBloc, SignInFormState>(
      listener: (context, state) {
        // always listens
        // if in [authFailureOrSuccess] some() arrives instead of none()
        // operations are performed for left and right
        // otherwise empty
        state.authFailureOrSuccess.fold(
          () {},
          (either) {
            either.fold(
              (f) {
                FlushbarMessenger.showFailureMessage(
                  context: context,
                  message: f.mapAuthFailure(),
                );
              },
              (r) {
                ChatsOverviewRoute().go(context);

                context
                    .read<AuthBloc>()
                    .add(const AuthEvent.authCheckRequested());
              },
            );
          },
        );
      },
      builder: (context, state) {
        return Form(
          autovalidateMode: state.showErrorMessages,
          child: Flexible(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Text('Авторизация', style: TextStyles.gilroy24Medium()),
                    Gap(20.r),
                    CustomTextFormField(
                      controller: emailController,
                      hintText: 'Email',
                      onChanged: (value) => context
                          .read<SignInFormBloc>()
                          .add(SignInFormEvent.emailChanged(value)),
                      validator: (_) => context
                          .read<SignInFormBloc>()
                          .state
                          .emailAddress
                          .value
                          .foldAuthValidation(),
                      textInputAction: TextInputAction.next,
                    ),
                    Gap(20.r),
                    CustomTextFormField(
                      controller: passwordController,
                      hintText: 'Пароль',
                      onChanged: (value) => context
                          .read<SignInFormBloc>()
                          .add(SignInFormEvent.passwordChanged(value)),
                      validator: (_) => context
                          .read<SignInFormBloc>()
                          .state
                          .password
                          .value
                          .foldAuthValidation(),
                      textInputAction: TextInputAction.done,
                      onEditingComplete: () => _login(context),
                    ),
                    Gap(20.r),
                    CustomButton(
                      text: 'Войти',
                      onTap: () => _login(context),
                      color: Colours.green,
                    ),
                    Gap(20.r),
                    Text.rich(
                      TextSpan(
                        text: 'Нет аккаунта? ',
                        style: TextStyles.gilroy14Medium(),
                        children: [
                          TextSpan(
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                context.push(SignUpPage.route);
                              },
                            text: 'Создайте!',
                            style: TextStyles.gilroy14Medium(
                              color: Colours.blue,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _login(BuildContext context) {
    context.read<SignInFormBloc>().add(
          const SignInFormEvent.signInWithEmailAndPasswordPressed(),
        );
  }
}
