import 'package:chat/application/auth/sign_up_form/sign_up_form_bloc.dart';
import 'package:chat/injection.dart';
import 'package:chat/presentation/auth/sign_up/widgets/sign_up_form.dart';
import 'package:chat/presentation/core/common_widgets/custom_loading_overlay_widget.dart';
import 'package:chat/presentation/core/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({super.key});

  static const route = '/sign_up';

  double get _padding => 10.r;

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<SignUpFormBloc>(),
      child: BlocBuilder<SignUpFormBloc, SignUpFormState>(
        buildWhen: (previous, current) =>
            previous.isSubmiting != current.isSubmiting,
        builder: (context, signUpFormState) {
          return Stack(
            children: [
              Scaffold(
                appBar: AppBar(
                  title: Text(
                    'Chat',
                    style: TextStyles.gilroy20Medium(),
                  ),
                  centerTitle: true,
                ),
                body: Padding(
                  padding: EdgeInsets.symmetric(horizontal: _padding),
                  child: SingleChildScrollView(
                    child: SizedBox(
                      height: 1.sh -
                          kToolbarHeight -
                          kTextTabBarHeight -
                          kBottomNavigationBarHeight -
                          _padding,
                      width: 1.sw,
                      child: const Column(
                        children: [
                          SignUpForm(),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              if (signUpFormState.isSubmiting)
                const CustomLoadingOverlayWidget(),
            ],
          );
        },
      ),
    );
  }
}
