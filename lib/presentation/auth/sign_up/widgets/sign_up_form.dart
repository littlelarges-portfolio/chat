// ignore_for_file: inference_failure_on_instance_creation

import 'package:chat/application/auth/auth_bloc.dart';
import 'package:chat/application/auth/sign_up_form/sign_up_form_bloc.dart';
import 'package:chat/presentation/auth/core/flushbar_messenger.dart';
import 'package:chat/presentation/auth/core/helpers.dart';
import 'package:chat/presentation/core/colours.dart';
import 'package:chat/presentation/core/common_widgets/custom_button_widget.dart';
import 'package:chat/presentation/core/common_widgets/custom_text_form_field_widget.dart';
import 'package:chat/presentation/core/routes/router.dart';
import 'package:chat/presentation/core/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';

class SignUpForm extends HookWidget {
  const SignUpForm({super.key});

  @override
  Widget build(BuildContext context) {
    final nameController = useTextEditingController();
    final emailController = useTextEditingController();
    final passwordController = useTextEditingController();
    final confirmPasswordController = useTextEditingController();

    return BlocConsumer<SignUpFormBloc, SignUpFormState>(
      listener: (context, state) {
        // always listens
        // if in [authFailureOrSuccess] some() arrives instead of none()
        // operations are performed for left and right
        // otherwise empty
        state.authFailureOrSuccess.fold(
          () {},
          (either) {
            either.fold(
              (f) {
                FlushbarMessenger.showFailureMessage(
                  context: context,
                  message: f.mapAuthFailure(),
                );
              },
              (r) {
                ChatsOverviewRoute().go(context);

                context
                    .read<AuthBloc>()
                    .add(const AuthEvent.authCheckRequested());
              },
            );
          },
        );
      },
      builder: (context, state) {
        return Form(
          autovalidateMode: state.showErrorMessages,
          child: Flexible(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  children: [
                    Text('Регистрация', style: TextStyles.gilroy24Medium()),
                    Gap(20.r),
                    CustomTextFormField(
                      controller: nameController,
                      hintText: 'Введите имя',
                      onChanged: (value) => context
                          .read<SignUpFormBloc>()
                          .add(SignUpFormEvent.nameChanged(value)),
                      validator: (_) => context
                          .read<SignUpFormBloc>()
                          .state
                          .name
                          .value
                          .foldAuthValidation(),
                      textInputAction: TextInputAction.next,
                    ),
                    Gap(20.r),
                    CustomTextFormField(
                      controller: emailController,
                      hintText: 'Email',
                      onChanged: (value) => context
                          .read<SignUpFormBloc>()
                          .add(SignUpFormEvent.emailChanged(value)),
                      validator: (_) => context
                          .read<SignUpFormBloc>()
                          .state
                          .emailAddress
                          .value
                          .foldAuthValidation(),
                      textInputAction: TextInputAction.next,
                    ),
                    Gap(20.r),
                    CustomTextFormField(
                      controller: passwordController,
                      hintText: 'Пароль',
                      onChanged: (value) => context
                          .read<SignUpFormBloc>()
                          .add(SignUpFormEvent.passwordChanged(value)),
                      validator: (_) => context
                          .read<SignUpFormBloc>()
                          .state
                          .password
                          .value
                          .foldAuthValidation(),
                      textInputAction: TextInputAction.next,
                    ),
                    Gap(20.r),
                    CustomTextFormField(
                      controller: confirmPasswordController,
                      hintText: 'Подтвердите пароль',
                      onChanged: (value) => context
                          .read<SignUpFormBloc>()
                          .add(SignUpFormEvent.confirmPasswordChanged(value)),
                      validator: (_) => context
                          .read<SignUpFormBloc>()
                          .state
                          .confirmPassword
                          .value
                          .foldAuthValidation(),
                      textInputAction: TextInputAction.done,
                      onEditingComplete: () => _register(context),
                    ),
                    Gap(20.r),
                    CustomButton(
                      text: 'Создать аккаунт',
                      onTap: () => _register(context),
                      color: Colours.blue,
                      textColor: Colours.white,
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void _register(BuildContext context) {
    context
        .read<SignUpFormBloc>()
        .add(const SignUpFormEvent.createAccountPressed());
  }
}
