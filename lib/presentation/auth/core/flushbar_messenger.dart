// ignore_for_file: inference_failure_on_instance_creation

import 'package:another_flushbar/flushbar.dart';
import 'package:chat/presentation/core/colours.dart';
import 'package:flutter/material.dart';

abstract class FlushbarMessenger {
  static const _defaultAnimationDuration = Duration(milliseconds: 300);
  static const _defaultDuration = Duration(seconds: 3);

  static void showSuccessMessage({
    required BuildContext context,
    required String message,
  }) {
    Flushbar(
      message: message,
      backgroundColor: Colours.blue,
      animationDuration: _defaultAnimationDuration,
      duration: _defaultDuration,
    ).show(context);
  }

  static void showFailureMessage({
    required BuildContext context,
    required String message,
  }) {
    Flushbar(
      message: message,
      backgroundColor: Colours.errorColor,
      animationDuration: _defaultAnimationDuration,
      duration: _defaultDuration,
    ).show(context);
  }

  static void showCustomMessage({
    required BuildContext context,
    required String message,
    Color? backgroundColor,
    Duration? animationDuration,
    Duration? duration,
  }) {
    Flushbar(
      message: message,
      backgroundColor: backgroundColor ?? Colours.gray,
      animationDuration: animationDuration ?? _defaultAnimationDuration,
      duration: duration ?? _defaultDuration,
    ).show(context);
  }
}
