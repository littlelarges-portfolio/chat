import 'package:chat/domain/auth/auth_failure.dart';
import 'package:chat/domain/core/errors/failures.dart';
import 'package:dartz/dartz.dart';

extension ValueFailureFolder<T> on Either<ValueFailure<T>, T> {
  String? foldAuthValidation() {
    return fold(
      (f) => f.map(
        auth: (authFailure) => authFailure.f.map(
          invalidEmail: (_) => 'Invalid email',
          shortPassword: (_) => 'Short password',
          cantBeEmpty: (_) => "Can't be empty",
        ),
      ),
      (_) => null,
    );
  }
}

extension AuthFailureMapper on AuthFailure {
  String mapAuthFailure() {
    return map(
      cancelledByUser: (_) => 'Cancelled',
      serverError: (_) => 'Server error',
      emailAlreadyInUse: (_) => 'Email already in use',
      invalidEmailAndPasswordCombination: (_) =>
          'Invalid email and password combination',
      accountDoesntExist: (_) => 'Account does not exist',
      unexpected: (_) => 'Unexpected error occurred',
    );
  }
}
