import 'package:chat/application/auth/auth_bloc.dart';
import 'package:chat/application/user/user_bloc.dart';
import 'package:chat/presentation/core/routes/router.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void authListener(BuildContext context, AuthState authState) {
  authState.map(
    initial: (_) {
      debugPrint('initial');
    },
    authenticated: (authenticatedState) {
      debugPrint('auth success ${authenticatedState.user}');
      context
          .read<UserBloc>()
          .add(UserEvent.updated(user: authenticatedState.user));

      ChatsOverviewRoute().go(context);
    },
    unauthenticated: (_) {
      debugPrint('auth failure');
      context.read<UserBloc>().add(const UserEvent.cleared());

      SignInRoute().go(context);
    },
  );
}
