import 'package:chat/domain/auth/value_objects.dart';
import 'package:chat/domain/core/value_objects.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'user.freezed.dart';

@freezed
abstract class User with _$User {
  const factory User({
    required UniqueId id,
    required Name name,
    required EmailAddress email,
  }) = _User;

  factory User.empty() => User(
        id: UniqueId.fromUniqueString(''),
        name: Name(''),
        email: EmailAddress(''),
      );
}
