import 'package:chat/domain/core/typedef/typedefs.dart';
import 'package:chat/domain/user/user.dart';

abstract class IUserRepository {
  Stream<RemoteUsers> watchUsers();
  Future<RemoteUserResult> create({required User user});
  Future<RemoteUserResult> update({required User user});
}
