import 'package:chat/domain/core/value_objects.dart';
import 'package:chat/domain/core/value_validators.dart';

class Name extends ValueObject<String> {
  factory Name(String input) =>
      Name._(validateEmpty(input));

  const Name._(super.value);
}

class EmailAddress extends ValueObject<String> {
  factory EmailAddress(String input) =>
      EmailAddress._(validateEmailAddress(input));

  const EmailAddress._(super.value);
}

class Password extends ValueObject<String> {
  factory Password(String input) =>
      Password._(validateEmpty(input).flatMap(validatePassword));

  const Password._(super.value);
}
