import 'package:chat/domain/auth/value_objects.dart';
import 'package:chat/domain/core/typedef/typedefs.dart';
import 'package:chat/domain/user/user.dart';
import 'package:dartz/dartz.dart';

abstract class IAuthFacade {
  Future<Option<User>> getSignedInUser();
  Future<AuthResult> registerWithEmailAndPassword({
    required Name name,
    required EmailAddress emailAddress,
    required Password password,
  });
  Future<AuthResult> signInWithEmailAndPassword({
    required EmailAddress emailAddress,
    required Password password,
  });
  Future<void> signOut();
}
