import 'package:chat/domain/auth/auth_failure.dart';
import 'package:chat/domain/chat/message.dart';
import 'package:chat/domain/chat/message_failure.dart';
import 'package:chat/domain/core/errors/failures.dart';
import 'package:chat/domain/user/user.dart';
import 'package:chat/domain/user/user_failure.dart';
import 'package:dartz/dartz.dart';
import 'package:kt_dart/kt.dart';

typedef Value<T> = Either<ValueFailure<T>, T>;

typedef AuthResult = Either<AuthFailure, Unit>;

typedef AuthOptionResult = Option<AuthResult>;

typedef RemoteMessages = Either<MessageFailure, KtList<Message>>;

typedef RemoteMessageResult = Either<MessageFailure, Unit>;

typedef RemoteUsers = Either<UserFailure, KtList<User>>;

typedef RemoteUserResult = Either<UserFailure, Unit>;
