import 'package:freezed_annotation/freezed_annotation.dart';

part 'failures.freezed.dart';

@freezed
sealed class ValueFailure<T> with _$ValueFailure<T> {
  const factory ValueFailure.auth(AuthValueFailure<T> f) = _Auth<T>;
}

@freezed
sealed class AuthValueFailure<T> with _$AuthValueFailure<T> {
  const factory AuthValueFailure.invalidEmail({required T failedValue}) =
      _InvalidEmail<T>;
  const factory AuthValueFailure.shortPassword({required T failedValue}) =
      _ShortPassword<T>;
  const factory AuthValueFailure.cantBeEmpty({required T failedValue}) =
      _CantBeEmpty<T>;
}
