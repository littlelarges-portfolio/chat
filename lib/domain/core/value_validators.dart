import 'package:chat/domain/core/errors/failures.dart';
import 'package:chat/domain/core/typedef/typedefs.dart';
import 'package:dartz/dartz.dart';

Value<String> validateEmailAddress(String input) {
  const emailRegex =
      r"""^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+""";

  if (RegExp(emailRegex).hasMatch(input)) {
    return right(input);
  } else {
    return left(
      ValueFailure.auth(
        AuthValueFailure.invalidEmail(failedValue: input),
      ),
    );
  }
}

Value<String> validatePassword(String input) {
  if (input.length >= 8) {
    return right(input);
  } else {
    return left(
      ValueFailure.auth(
        AuthValueFailure.shortPassword(failedValue: input),
      ),
    );
  }
}

Value<String> validateEmpty(String input) {
  if (input.isNotEmpty) {
    return right(input);
  } else {
    return left(
      ValueFailure.auth(
        AuthValueFailure.cantBeEmpty(failedValue: input),
      ),
    );
  }
}
