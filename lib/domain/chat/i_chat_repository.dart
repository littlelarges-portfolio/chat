import 'package:chat/domain/core/typedef/typedefs.dart';
import 'package:chat/infrastructure/chat/chat.dart';

abstract class IChatRepository {
  Stream<RemoteMessages> watchMessages({required Chat chat});
  // Future<MessageRemoteResult> sendMessage({required Message message});
}
