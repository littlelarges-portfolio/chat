import 'package:chat/domain/user/user.dart' as u;
import 'package:chat/infrastructure/auth/user_dtos.dart';
import 'package:chat/infrastructure/core/firestore_helpers.dart';
import 'package:chat/injection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

extension FirebaseUserDomainX on User {
  Future<u.User> toDomain() async {
    final userDocumentSnapshot =
        await getIt<FirebaseFirestore>().usersCollection.doc(uid).get();

    if (userDocumentSnapshot.exists) {
      final userDocumentJson =
          userDocumentSnapshot.data()! as Map<String, dynamic>;

      return UserDto.fromJson(userDocumentJson).toDomain();
    }

    return u.User.empty();
  }
}
