import 'package:chat/domain/core/typedef/typedefs.dart';
import 'package:chat/domain/user/i_user_repository.dart';
import 'package:chat/domain/user/user.dart';
import 'package:chat/domain/user/user_failure.dart';
import 'package:chat/infrastructure/auth/user_dtos.dart';
import 'package:chat/infrastructure/core/firestore_helpers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:rxdart/rxdart.dart';

@LazySingleton(as: IUserRepository)
class UserRepository implements IUserRepository {
  const UserRepository(this._firestore);

  final FirebaseFirestore _firestore;

  @override
  Stream<RemoteUsers> watchUsers() async* {
    yield* _firestore.usersCollection
        .orderBy('name')
        .snapshots()
        .map(
          (snapshot) => right<UserFailure, KtList<User>>(
            snapshot.docs
                .map(
                  (user) => UserDto.fromFirestore(
                    user as QueryDocumentSnapshot<Map<String, dynamic>>,
                  ).toDomain(),
                )
                .toImmutableList(),
          ),
        )
        .onErrorReturnWith((e, stackTrace) {
      if (e is FirebaseException) {
        return left(const UserFailure.serverError());
      } else {
        return left(const UserFailure.unexpected());
      }
    });
  }

  @override
  Future<RemoteUserResult> create({required User user}) async {
    try {
      await _firestore.usersCollection.doc(user.id.getOrCrash()).set(
            UserDto.fromDomain(user).toJson(),
          );

      return right(unit);
    } on FirebaseException catch (_) {
      return left(const UserFailure.serverError());
    } catch (_) {
      return left(const UserFailure.unexpected());
    }
  }

  @override
  Future<RemoteUserResult> update({required User user}) async {
    try {
      await _firestore.usersCollection.doc(user.id.getOrCrash()).update(
            UserDto.fromDomain(user).toJson(),
          );

      return right(unit);
    } on FirebaseException catch (_) {
      return left(const UserFailure.serverError());
    } catch (_) {
      return left(const UserFailure.unexpected());
    }
  }
}
