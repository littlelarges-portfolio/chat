import 'package:chat/domain/auth/i_auth_facade.dart';
import 'package:chat/domain/core/errors/errors.dart.dart';
import 'package:chat/injection.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

extension FirestoreX on FirebaseFirestore {
  Future<DocumentReference> userDocument() async {
    final userOption = await getIt<IAuthFacade>().getSignedInUser();
    final user = userOption.getOrElse(() => throw NotAuthenticatedError());
    return getIt<FirebaseFirestore>()
        .collection('users')
        .doc(user.id.getOrCrash());
  }

  CollectionReference get chatsCollection => collection('chats');
  CollectionReference get usersCollection => collection('users');
}

extension DocumentReferenceX on DocumentReference {
  CollectionReference get messagesCollection => collection('messages');
}
