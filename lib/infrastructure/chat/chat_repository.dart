import 'package:chat/domain/chat/i_chat_repository.dart';
import 'package:chat/domain/chat/message.dart';
import 'package:chat/domain/chat/message_failure.dart';
import 'package:chat/domain/core/typedef/typedefs.dart';
import 'package:chat/infrastructure/chat/chat.dart';
import 'package:chat/infrastructure/chat/message_dtos.dart';
import 'package:chat/infrastructure/core/firestore_helpers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/services.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:rxdart/rxdart.dart';

@LazySingleton(as: IChatRepository)
class ChatRepository implements IChatRepository {
  ChatRepository(this._firestore);

  final FirebaseFirestore _firestore;

  @override
  Stream<RemoteMessages> watchMessages({required Chat chat}) async* {
    yield* _firestore.chatsCollection
        .doc(chat.id)
        .messagesCollection
        .orderBy('serverTimeStamp', descending: true)
        .snapshots()
        .map(
          (snapshot) => right<MessageFailure, KtList<Message>>(
            snapshot.docs.map((doc) {
              return MessageDto.fromFirestore(
                doc as QueryDocumentSnapshot<Map<String, dynamic>>,
              ).toDomain();
            }).toImmutableList(),
          ),
        )
        .onErrorReturnWith((e, stackTrace) {
      if (e is PlatformException && e.message!.contains('ACCESS_DENIED')) {
        return left(const MessageFailure.insufficientPermission());
      } else {
        return left(const MessageFailure.unexpected());
      }
    });
  }

  // @override
  // Future<MessageRemoteResult> sendMessage({
  //   required User receiver,
  //   required Message message,
  // }) async {
  //   final userDocument = await _firestore.userDocument();
  //   final userInfoSnapshot = await userDocument.get();
  //   final userInfoData = userInfoSnapshot.data();

  //   if (userInfoData != null) {
  //     final userInfoMap = userInfoData as Map<String, dynamic>;
  //     final currentUser = User(
  //       id: UniqueId.fromUniqueString(userDocument.id),
  //       name: Name(userInfoData['name'] as String),
  //       email: EmailAddress(userInfoMap['email'] as String),
  //     );
  //   }
  // }
}
