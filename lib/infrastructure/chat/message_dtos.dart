// ignore_for_file: invalid_annotation_target

import 'package:chat/domain/chat/message.dart';
import 'package:chat/infrastructure/auth/user_dtos.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'message_dtos.freezed.dart';
part 'message_dtos.g.dart';

@freezed
abstract class MessageDto implements _$MessageDto {
  const factory MessageDto({
    required String id,
    required UserDto sender,
    required UserDto receiver,
    required String message,
    required DateTime timestamp,
  }) = _MessageDto;

  factory MessageDto.fromJson(Map<String, dynamic> json) =>
      _$MessageDtoFromJson(json);

  factory MessageDto.fromFirestore(
    QueryDocumentSnapshot<Map<String, dynamic>> doc,
  ) {
    return MessageDto.fromJson(doc.data()).copyWith(id: doc.id);
  }

  const MessageDto._();

  factory MessageDto.fromDomain(Message message) {
    return MessageDto(
      id: message.id,
      sender: UserDto.fromDomain(message.sender),
      receiver: UserDto.fromDomain(message.receiver),
      message: message.message,
      timestamp: message.timestamp.toDate(),
    );
  }

  Message toDomain() {
    return Message(
      id: id,
      sender: sender.toDomain(),
      receiver: receiver.toDomain(),
      message: message,
      timestamp: Timestamp.fromDate(timestamp),
    );
  }
}
