// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:chat/application/auth/auth_bloc.dart' as _i14;
import 'package:chat/application/auth/sign_in_form/sign_in_form_bloc.dart'
    as _i15;
import 'package:chat/application/auth/sign_up_form/sign_up_form_bloc.dart'
    as _i16;
import 'package:chat/application/user/actor/user_actor_bloc.dart' as _i10;
import 'package:chat/application/user/user_bloc.dart' as _i3;
import 'package:chat/application/users/watcher/users_watcher_bloc.dart' as _i11;
import 'package:chat/domain/auth/i_auth_facade.dart' as _i12;
import 'package:chat/domain/chat/i_chat_repository.dart' as _i6;
import 'package:chat/domain/user/i_user_repository.dart' as _i8;
import 'package:chat/infrastructure/auth/firebase_auth_facade.dart' as _i13;
import 'package:chat/infrastructure/chat/chat_repository.dart' as _i7;
import 'package:chat/infrastructure/core/firebase_injectable_module.dart'
    as _i17;
import 'package:chat/infrastructure/user/user_repository.dart' as _i9;
import 'package:cloud_firestore/cloud_firestore.dart' as _i5;
import 'package:firebase_auth/firebase_auth.dart' as _i4;
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

extension GetItInjectableX on _i1.GetIt {
// initializes the registration of main-scope dependencies inside of GetIt
  _i1.GetIt init({
    String? environment,
    _i2.EnvironmentFilter? environmentFilter,
  }) {
    final gh = _i2.GetItHelper(
      this,
      environment,
      environmentFilter,
    );
    final firebaseInjectableModule = _$FirebaseInjectableModule();
    gh.factory<_i3.UserBloc>(() => _i3.UserBloc());
    gh.lazySingleton<_i4.FirebaseAuth>(
        () => firebaseInjectableModule.firebaseAuth);
    gh.lazySingleton<_i5.FirebaseFirestore>(
        () => firebaseInjectableModule.firestore);
    gh.lazySingleton<_i6.IChatRepository>(
        () => _i7.ChatRepository(gh<_i5.FirebaseFirestore>()));
    gh.lazySingleton<_i8.IUserRepository>(
        () => _i9.UserRepository(gh<_i5.FirebaseFirestore>()));
    gh.factory<_i10.UserActorBloc>(
        () => _i10.UserActorBloc(gh<_i8.IUserRepository>()));
    gh.factory<_i11.UsersWatcherBloc>(
        () => _i11.UsersWatcherBloc(gh<_i8.IUserRepository>()));
    gh.lazySingleton<_i12.IAuthFacade>(() => _i13.FirebaseAuthFacade(
          gh<_i4.FirebaseAuth>(),
          gh<_i8.IUserRepository>(),
        ));
    gh.factory<_i14.AuthBloc>(() => _i14.AuthBloc(gh<_i12.IAuthFacade>()));
    gh.factory<_i15.SignInFormBloc>(
        () => _i15.SignInFormBloc(gh<_i12.IAuthFacade>()));
    gh.factory<_i16.SignUpFormBloc>(
        () => _i16.SignUpFormBloc(gh<_i12.IAuthFacade>()));
    return this;
  }
}

class _$FirebaseInjectableModule extends _i17.FirebaseInjectableModule {}
