part of 'user_bloc.dart';

@freezed
class UserEvent with _$UserEvent {
  const factory UserEvent.updated({required User user}) = _Updated;
  const factory UserEvent.cleared() = _Cleared;
}
