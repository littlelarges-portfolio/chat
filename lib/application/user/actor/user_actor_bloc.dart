import 'package:bloc/bloc.dart';
import 'package:chat/domain/user/i_user_repository.dart';
import 'package:chat/domain/user/user.dart';
import 'package:chat/domain/user/user_failure.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'user_actor_event.dart';
part 'user_actor_state.dart';
part 'user_actor_bloc.freezed.dart';

@injectable
class UserActorBloc extends Bloc<UserActorEvent, UserActorState> {
  UserActorBloc(this._userRepository) : super(const UserActorState.initial()) {
    on<UserActorEvent>((event, emit) async {
      await event.map(
        updated: (e) async {
          emit(const UserActorState.actionInProgress());

          final possibleFailure = await _userRepository.update(user: e.user);

          possibleFailure.fold(
            (f) => emit(UserActorState.updateFailure(failure: f)),
            (r) => emit(const UserActorState.updateSuccess()),
          );
        },
      );
    });
  }

  final IUserRepository _userRepository;
}
