part of 'user_actor_bloc.dart';

@freezed
class UserActorState with _$UserActorState {
  const factory UserActorState.initial() = _Initial;
  const factory UserActorState.actionInProgress() = _ActionInProgress;
  const factory UserActorState.updateSuccess() = _UpdateSuccess;
  const factory UserActorState.updateFailure({required UserFailure failure}) =
      _UpdateFailure;
}
