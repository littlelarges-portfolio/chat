import 'package:bloc/bloc.dart';
import 'package:chat/domain/user/user.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'user_event.dart';
part 'user_state.dart';
part 'user_bloc.freezed.dart';

@injectable
class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(const UserState.initial()) {
    on<UserEvent>((event, emit) {
      event.map(
        updated: (e) {
          emit(UserState.updatedSuccess(user: e.user));
        },
        cleared: (_) {
          emit(const UserState.initial());
        },
      );
    });
  }
}
