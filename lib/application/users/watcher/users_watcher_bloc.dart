import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:chat/domain/core/typedef/typedefs.dart';
import 'package:chat/domain/user/i_user_repository.dart';
import 'package:chat/domain/user/user.dart';
import 'package:chat/domain/user/user_failure.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';

part 'users_watcher_event.dart';
part 'users_watcher_state.dart';
part 'users_watcher_bloc.freezed.dart';

@injectable
class UsersWatcherBloc extends Bloc<UsersWatcherEvent, UsersWatcherState> {
  UsersWatcherBloc(this._userRepository) : super(const _Initial()) {
    on<UsersWatcherEvent>((event, emit) {
      event.map(
        watchStarted: (e) {
          emit(const UsersWatcherState.loadInProgress());
          _usersStreamSubscription?.cancel();
          _usersStreamSubscription = _userRepository.watchUsers().listen(
                (failureOrUsers) =>
                    add(UsersWatcherEvent.usersReceived(failureOrUsers)),
              );
        },
        usersReceived: (e) {
          e.failureOrUsers.fold(
            (f) => emit(UsersWatcherState.loadFailure(f)),
            (users) => emit(UsersWatcherState.loadSuccess(users)),
          );
        },
      );
    });
  }

  @override
  Future<void> close() async {
    await _usersStreamSubscription?.cancel();
    return super.close();
  }

  final IUserRepository _userRepository;
  StreamSubscription<RemoteUsers>? _usersStreamSubscription;
}
