part of 'users_watcher_bloc.dart';

@freezed
class UsersWatcherEvent with _$UsersWatcherEvent {
  const factory UsersWatcherEvent.watchStarted() = _WatchStarted;
  const factory UsersWatcherEvent.usersReceived(RemoteUsers failureOrUsers) =
      _UsersReceived;
}
