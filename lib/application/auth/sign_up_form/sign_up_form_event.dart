part of 'sign_up_form_bloc.dart';

@freezed
sealed class SignUpFormEvent with _$SignUpFormEvent {
  const factory SignUpFormEvent.nameChanged(String nameStr) = _NameChanged;
  const factory SignUpFormEvent.emailChanged(String emailStr) = _EmailChanged;
  const factory SignUpFormEvent.passwordChanged(String passwordStr) =
      _PasswordChanged;
  const factory SignUpFormEvent.confirmPasswordChanged(
    String confirmPasswordStr,
  ) = _ConfirmPasswordChanged;
  const factory SignUpFormEvent.createAccountPressed() = _CreateAccountPressed;
}
