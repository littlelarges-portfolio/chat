import 'package:bloc/bloc.dart';
import 'package:chat/domain/auth/auth_failure.dart';
import 'package:chat/domain/auth/i_auth_facade.dart';
import 'package:chat/domain/auth/value_objects.dart';
import 'package:chat/domain/core/typedef/typedefs.dart';
import 'package:chat/domain/core/value_objects.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';

part 'sign_up_form_bloc.freezed.dart';
part 'sign_up_form_event.dart';
part 'sign_up_form_state.dart';

@injectable
class SignUpFormBloc extends Bloc<SignUpFormEvent, SignUpFormState> {
  SignUpFormBloc(this._authFacade) : super(SignUpFormState.initial()) {
    on<SignUpFormEvent>((event, emit) async {
      await event.map(
        nameChanged: (e) {
          emit(
            state.copyWith(
              name: Name(e.nameStr),
              authFailureOrSuccess: none(),
            ),
          );
        },
        emailChanged: (e) {
          emit(
            state.copyWith(
              emailAddress: EmailAddress(e.emailStr),
              authFailureOrSuccess: none(),
            ),
          );
        },
        passwordChanged: (e) {
          emit(
            state.copyWith(
              password: Password(e.passwordStr),
              authFailureOrSuccess: none(),
            ),
          );
        },
        confirmPasswordChanged: (e) {
          emit(
            state.copyWith(
              confirmPassword: Password(e.confirmPasswordStr),
              authFailureOrSuccess: none(),
            ),
          );
        },
        createAccountPressed: (e) async {
          AuthResult? failureOrSuccess;

          final isEmailValid = state.emailAddress.isValid();
          final isPasswordValid = state.password.isValid();

          if (isEmailValid && isPasswordValid) {
            emit(
              state.copyWith(
                isSubmiting: true,
                authFailureOrSuccess: none(),
              ),
            );

            failureOrSuccess = await _authFacade.registerWithEmailAndPassword(
              emailAddress: state.emailAddress,
              password: state.password,
              name: state.name,
            );
          }

          emit(
            state.copyWith(
              isSubmiting: false,
              showErrorMessages: AutovalidateMode.always,
              authFailureOrSuccess: optionOf(failureOrSuccess),
            ),
          );
        },
      );
    });
  }

  final IAuthFacade _authFacade;
}
