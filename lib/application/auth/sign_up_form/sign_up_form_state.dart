part of 'sign_up_form_bloc.dart';

@freezed

/// [isSubmiting] is used to indicate should be shown circular indicator
///
/// [showErrorMessages] is used to indicate whether errors should be shown
/// if they exist in [ValueObject]s
///
/// [authFailureOrSuccess] is used to ensure that there are no errors
/// when you first enter the login page
///
class SignUpFormState with _$SignUpFormState {
  const factory SignUpFormState({
    required Name name,
    required EmailAddress emailAddress,
    required Password password,
    required Password confirmPassword,
    required AutovalidateMode showErrorMessages,
    required bool isSubmiting,
    required AuthOptionResult authFailureOrSuccess,
  }) = _SignUpFormState;

  factory SignUpFormState.initial() => SignUpFormState(
        name: Name(''),
        emailAddress: EmailAddress(''),
        password: Password(''),
        confirmPassword: Password(''),
        showErrorMessages: AutovalidateMode.disabled,
        isSubmiting: false,
        authFailureOrSuccess: none(),
      );
}
